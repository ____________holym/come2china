PACKAGE_NAME = come2china
ADDON_SDK_VERSION = git
export ADDON_SDK_VERSION

CFX_BIN = tools/sdk-cfx
CFX_OPTIONS = --strip-sdk
CFX_OPTIONS_LOCAL = --overload-modules
PROFILE = profiles/$(PACKAGE_NAME)
VERSION = $(shell sed -e '/"version":/!d' \
	-e 's/.*: "\([^"]*\)".*/\1/' \
	-e q package.json)
$(info $(PACKAGE_NAME) VERSION = $(VERSION))
PACKAGE_DIR = $(PACKAGE_NAME)-$(VERSION)
PACKAGE_DATA = Makefile README.md data doc \
			   lib package.json test tools \
			   zhong.png zhong64.png zhong128.png

define allMsg 
Known targets:
	run
	run-local
	xpi
	xpi-comp
	xpi-mobile
	dist
endef
export allMsg

all:
	@echo "$$allMsg"

run:
	$(CFX_BIN) run $(CFX_OPTIONS) -p $(PROFILE)

run-local:
	$(CFX_BIN) run $(CFX_OPTIONS_LOCAL) -p $(PROFILE)

xpi:
	$(CFX_BIN) xpi $(CFX_OPTIONS)
	/bin/mv $(PACKAGE_NAME).xpi $(PACKAGE_NAME)-$(VERSION).xpi 

xpi-comp:
	$(CFX_BIN) xpi 
	/bin/mv $(PACKAGE_NAME).xpi $(PACKAGE_NAME)-oldfox-$(VERSION).xpi 

xpi-mobile:
	$(CFX_BIN) xpi $(CFX_OPTIONS) --force-mobile
	/bin/mv $(PACKAGE_NAME).xpi $(PACKAGE_NAME)-mobile-$(VERSION).xpi 

dist:
	rm -rf dist/$(PACKAGE_DIR)
	mkdir -p dist/$(PACKAGE_DIR)
	cp -al $(PACKAGE_DATA) dist/$(PACKAGE_DIR) 
	cd dist &&\
		tar --exclude="*.sw[po]" -Jcvf $(PACKAGE_DIR).tar.xz $(PACKAGE_DIR) 
	@echo "Save to dist/$(PACKAGE_DIR).tar.xz"

.PHONY: run run-local xpi xpi-comp xpi-mobile dist
