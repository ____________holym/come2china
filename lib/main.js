/*
 * Copyright 2012 - 2014 mozbugbox <mozbugbox@yahoo.com.au>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// The main module of the come2china Add-on.
// Inspired by/copied: https://github.com/zhuzhuor/Unblock-Youku

console.debug = function() {}; // Turn off debug

const data = require("sdk/self").data;
const events = require("sdk/system/events");
const xulApp = require("sdk/system/xul-app");
const unload = require("sdk/system/unload");

const header = require("./header");
const Proxy2 = require("./proxy").Proxy2;

C2C_ICONS = {
    on: data.url("zhong.png"),
    off: data.url("zhong-disable.png")
};

function Control() {
    var this_control = this;
    this.status_icon = null;
    this.mobile_menu = null;

    var widget_info = {
        // Mandatory string used to identify your widget in order to
        // save its location when the user moves it in the browser.
        // This string has to be unique and must not be changed over time.
        id: "come2china-main-icon",

        // A required string description of the widget used for
        // accessibility, title bars, and error reporting.
        label: "Come to China!",
        tooltip: "Click and Come to China...",


        // An optional string URL to content to load into the widget.
        // This can be local content or remote content, an image or
        // web content. Widgets must have either the content property
        // or the contentURL property set.
        //
        // If the content is an image, it is automatically scaled to
        // be 16x16 pixels.
        contentURL: C2C_ICONS.off,

        // Add a function to trigger when the Widget is clicked.
        onClick: function(event) {
            console.debug("header.sogou_proxy:", header.sogou_proxy);
            this_control.toggle(header.sogou_proxy.activated);
        }
    };

    if (xulApp.is("Fennec")) {
        const MobileMenu = require("./mobile-menu").MobileMenu;

        var menu_options = {
            name: widget_info.label,
            icon: C2C_ICONS.on,
            callback: widget_info.onClick,
            checkable: true,
            checked: false
        };
        this.mobile_menu = new MobileMenu(menu_options);
    } else {
        const Widget = require("sdk/widget").Widget;
        this.status_icon = new Widget(widget_info);
    }
    unload.ensure(this);
}

Control.prototype = {
    signal: "http-on-modify-request",
    proxy: new Proxy2(),

    toggle: function(state) {
        console.debug("toggle state:", state);
        var icon_data;
        var label;

        header.sogou_proxy.activated = !state;
        if (header.sogou_proxy.activated === true) {
            // Turn on proxy, set preferences...
            icon_data = C2C_ICONS.on;
            label = "Come to China!";

            events.on(this.signal, header.modify_header);
            this.proxy.install();

        } else {
            icon_data = C2C_ICONS.off;
            label = "Click and Come to China...";

            events.off(this.signal, header.modify_header);
            this.proxy.restore();
        }

        if (this.status_icon !== null) {
            this.status_icon.contentURL = icon_data;
            this.status_icon.tooltip = label;
        }

        if (this.mobile_menu !== null) {
            this.mobile_menu.set_checked(header.sogou_proxy.activated);
        }
    },

    // On module unload.ensure()
    unload: function(reason) {
        console.debug("unloading control...");
        events.off(this.signal, header.modify_header);
        this.proxy.restore();
    }
};

function main(options, callbacks) {
    header.init_proxy();

    console.debug("loading main...");
    var control = new Control();
}

function onUnload(reason) {
    console.debug("done module unload: " + reason);
}

exports.main = main;
exports.onUnload = onUnload;

