/*
 * Copyright 2012 - 2014 mozbugbox <mozbugbox@yahoo.com.au>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This file set the proxy on/off
 */

const { Ci, Cc } = require("chrome");

function get_sogou_proxy(do_edu) {
    var range = 16 + 16; // 0 ~ 15 edu and 0 ~ 15 dxt
    var offset = 0;
    if (do_edu == true) {
        range = 16;
        offset = 0;
    } else if (do_edu == false) {
        range = 16;
        offset = 16;
    }

    console.debug("do_edu:", do_edu, "range:", range, "offset:", offset);
    var random_num = Math.floor(Math.random() * (range)) + offset;
    var proxy_addr;
    if (random_num < 16)
        proxy_addr = 'h' + random_num + '.dxt.bj.ie.sogou.com';  // 0 ~ 15
    else
        proxy_addr = 'h' + (random_num - 16) + '.edu.bj.ie.sogou.com';
        // (16 ~ 31) - 16
    // ctc and cnc do not work; don't know why
    var proxy_port = 80;

    var sogou_settings = [proxy_addr, proxy_port, 1];
    console.debug('proxy server:', proxy_addr, "port:", proxy_port);
    return sogou_settings;
}

// Force Proxy by setting the browser preference values.
function Proxy() {}
Proxy.prototype = {
    proxy_settings: ["network.proxy.http",
                     "network.proxy.http_port",
                     "network.proxy.type"],
    sogou_settings: null,
    firefox_settings: [],

    install: function() {
        var prefs = require("sdk/preferences/service");
        this.sogou_settings = get_sogou_proxy();
        for (var i = 0; i < this.proxy_settings.length; i++) {
            var k = this.proxy_settings[i];
            var v = this.sogou_settings[i];
            this.firefox_settings[i] = prefs.get(k);
            prefs.set(k, v);
        }
    },

    restore: function() {
        var prefs = require("sdk/preferences/service");
        for (var i = 0; i < this.proxy_settings.length; i++) {
            var k = this.proxy_settings[i];
            var v = this.firefox_settings[i];
            prefs.set(k, v);
        }
    }
};

exports.Proxy = Proxy;

const pS = Cc['@mozilla.org/network/protocol-proxy-service;1'].getService(Ci.nsIProtocolProxyService);

// Go through Proxy without setting the browser preference.
// We add proxy filter to ProtocolProxyService to add our own proxy object.
function Proxy2() {}
Proxy2.prototype = {
    proxy_info: null,
    install: function() {
        this.proxy_info = get_sogou_proxy();
        pS.registerFilter(this, 0);
    },

    restore: function() {
        pS.unregisterFilter(this);
    },

    create_proxyinfo: function(aproxyserver) {
        const PR_UINT32_MAX = 0xffffffff;
        var aproxy = aproxyserver.newProxyInfo("http", this.proxy_info[0],
                this.proxy_info[1], 0, PR_UINT32_MAX, null);
        return aproxy;
    },

    applyFilter: function(aproxyserver, auri, aproxyinfo) {
        var aproxy = aproxyinfo;
        if (auri.schemeIs("http")) {
            aproxy = this.create_proxyinfo(aproxyserver);
            console.debug("Using my proxy filter:", auri.spec);
        }
        return aproxy;
    }
};

exports.Proxy2 = Proxy2;

