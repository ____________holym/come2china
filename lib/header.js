/*
 * Copyright 2012 - 2014 mozbugbox <mozbugbox@yahoo.com.au>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Setup extra header sent to proxy for some kind of authentication.
 */

var sogou_proxy = {};  // namespace
const { Ci } = require("chrome");

exports.sogou_proxy = sogou_proxy;

function init_proxy() {
    sogou_proxy.activated = false;
    sogou_proxy.ip_addr = '220.181.1';
    sogou_proxy.ip_addr += Math.floor(Math.random() * 54 + 1); // 1 ~ 54
    sogou_proxy.ip_addr += '.';
    sogou_proxy.ip_addr += Math.floor(Math.random() * 254 + 1); // 1 ~ 254
    console.debug('faked ip addr: ' + sogou_proxy.ip_addr);

    sogou_proxy.sogou_auth = '/30/853edc6d49ba4e27';
    var tmp_str;
    for (var i = 0; i < 8; i++) {
        tmp_str = ('0000' +
            Math.floor(Math.random() * 65536).toString(16)).slice(-4);
        sogou_proxy.sogou_auth = tmp_str.toUpperCase() + sogou_proxy.sogou_auth;
    }
    console.debug('sogou_auth: ' + sogou_proxy.sogou_auth);
}
exports.init_proxy = init_proxy;

/*
   evt has three attributes:
       type: the event type name
       subject: the event subject object
       data: the event data string
   ohttp: a nsIHttpChannel object
   https://developer.mozilla.org/en/XPCOM_Interface_Reference
*/
function modify_header(evt) {
    var ohttp = evt.subject.QueryInterface(Ci.nsIHttpChannel);
    if (sogou_proxy.activated == false) {
        return;
    }
    //var timestamp = Math.round(details.timeStamp / 1000).toString(16);
    //var target_host = details.url.match(/:\/\/(.[^\/]+)/)[1];
    var timestamp = Math.round(Date.now() / 1000).toString(16);
    var target_host = ohttp.URI.host;

    var tag = compute_sogou_tag(timestamp + target_host + 'SogouExplorerProxy');

    console.debug("timestamp:", timestamp, "target_host:",
        target_host, "tag:", tag);

    ohttp.setRequestHeader('X-Sogou-Auth', sogou_proxy.sogou_auth, false);
    ohttp.setRequestHeader('X-Sogou-Timestamp', timestamp, false);
    ohttp.setRequestHeader('X-Sogou-Tag', tag, false);
    ohttp.setRequestHeader('X-Forwarded-For', sogou_proxy.ip_addr, false);
}

exports.modify_header = modify_header;

// based on http://xiaoxia.org/2011/03/10/depressed-research-about-sogou-proxy-server-authentication-protocol/
function compute_sogou_tag(s) {
    var total_len = s.length;
    var numb_iter = Math.floor(total_len / 4);
    var numb_left = total_len % 4;

    var hash = total_len;  // output hash tag

    for (var i = 0; i < numb_iter; i++) {
        // right most 16 bits in little-endian
        var low = s.charCodeAt(4 * i + 1) * 256 + s.charCodeAt(4 * i);
        // left most
        var high = s.charCodeAt(4 * i + 3) * 256 + s.charCodeAt(4 * i + 2);

        hash += low;
        hash %= 0x100000000;
        hash ^= hash << 16;

        hash ^= high << 11;
        hash += hash >>> 11;
        hash %= 0x100000000;
    }

    switch (numb_left) {
    case 3:
        hash += (s.charCodeAt(total_len - 2) << 8) + s.charCodeAt(
            total_len - 3);
        hash %= 0x100000000;
        hash ^= hash << 16;
        hash ^= s.charCodeAt(total_len - 1) << 18;
        hash += hash >>> 11;
        hash %= 0x100000000;
        break;
    case 2:
        hash += (s.charCodeAt(total_len - 1) << 8) + s.charCodeAt(
            total_len - 2);
        hash %= 0x100000000;
        hash ^= hash << 11;
        hash += hash >>> 17;
        hash %= 0x100000000;
        break;
    case 1:
        hash += s.charCodeAt(total_len - 1);
        hash %= 0x100000000;
        hash ^= hash << 10;
        hash += hash >>> 1;
        hash %= 0x100000000;
        break;
    default:
        break;
    }

    hash ^= hash << 3;
    hash += hash >>> 5;
    hash %= 0x100000000;

    hash ^= hash << 4;
    hash += hash >>> 17;
    hash %= 0x100000000;

    hash ^= hash << 25;
    hash += hash >>> 6;
    hash %= 0x100000000;

    // learnt from http://stackoverflow.com/questions/6798111/bitwise-operations-on-32-bit-unsigned-ints
    hash = hash >>> 0;

    return ('00000000' + hash.toString(16)).slice(-8);
}
