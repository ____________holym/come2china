/*
 * Copyright 2012 - 2014 mozbugbox <mozbugbox@yahoo.com.au>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Attach menu to mobile firefox
const winutils = require("sdk/window/utils");
const events = require("sdk/system/events");
const unload = require("sdk/system/unload");
const lutils = require("./local-utils");

/***
 * Create a mobile menu and attach to all the windows
 * @this {MobileMenu}
 * @param {map} menu_options = {
 *       name: widget_info.label,
 *       icon: widget_info.contentURL,
 *       callback: widget_info.onClick,
 *       checkable: true
 *   }
**/
function MobileMenu(menu_options) {
    var this_control = this;
    this.id_map = {}; // Map menu_id to window outerID
    unload.ensure(this);

    // icon has to point to file://path
    var icon_key = "icon";
    if (icon_key in menu_options) {
        var icon_file = lutils.toFilename(menu_options[icon_key]);
        menu_options[icon_key] = icon_file;
    }

    // Listen to further opened windows
    events.on("toplevel-window-ready", function(evt) {
        var new_win = evt.subject;
        var mid = this_control.attach_menu(new_win, menu_options);
        return true;
    }, true);

    winutils.windows().forEach(function(win) {
        var mid = this_control.attach_menu(win, menu_options);
    });
}

MobileMenu.prototype = {
    // Attach a menu to a given window
    attach_menu: function(win, options) {
        var nw = win.NativeWindow;
        var win_id = winutils.getOuterId(win);
        var menu_id = nw.menu.add(options);
        if (options.checkable === true) {
            if (options.checked) {
                nw.menu.update(menu_id, {checked: true});
            } else {
                nw.menu.update(menu_id, {checked: false});
            }
        }
        this.id_map[win_id] = menu_id;
        return menu_id;
    },

    set_checked: function(is_checked) {
        var this_control = this;
        // change check state of menu on all the windows.
        winutils.windows().forEach(function(win) {
            var win_id = winutils.getOuterId(win);
            var menu_id = this_control.id_map[win_id];
            var nw = win.NativeWindow;
            var m_opt = {checked: is_checked};
            nw.menu.update(menu_id, m_opt);
        });
    },

    unload: function(reason) {
        // detach menuitem from menubar
        var this_control = this;
        winutils.windows().forEach(function(win) {
            var win_id = winutils.getOuterId(win);
            var menu_id = this_control.id_map[win_id];
            if (menu_id) {
                var nw = win.NativeWindow;
                nw.menu.remove(menu_id);
            }
        });
    }
};

exports.MobileMenu = MobileMenu;

