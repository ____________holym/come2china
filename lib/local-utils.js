/*
 * Copyright 2012 - 2014 mozbugbox <mozbugbox@yahoo.com.au>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This file includes some miscellaneous utility functions
 */

const {Cc, Ci} = require("chrome");

/*
 * Convert resource:// or chrome:// to file path. See:
 * https://developer.mozilla.org/en-US/docs/Extensions/Mobile/API/NativeWindow/menu/add
 * and
 * https://bugzilla.mozilla.org/show_bug.cgi?id=788025
 **/
function toFilename(aURI) {
    const ios = Cc['@mozilla.org/network/io-service;1'].getService(
        Ci.nsIIOService);
    try {
        var uri = ios.newURI(aURI, null, null);
        if (aURI.startsWith("chrome://")) {
            let registry = Cc['@mozilla.org/chrome/chrome-registry;1'].
                getService(Ci["nsIChromeRegistry"]);
            return registry.convertChromeURL(uri).spec;
        } else if (aURI.startsWith("resource://")) {
            let handler = ios.getProtocolHandler("resource").QueryInterface(
                Ci.nsIResProtocolHandler);
            return handler.resolveURI(uri);
        }
    } catch (e) {
        throw new Error("Bad URI: " + aURI);
    }
    return aURI;
}

exports.toFilename = toFilename;
